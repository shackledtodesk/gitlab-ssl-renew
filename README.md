## [GitLab](https://gitlab.com/)/[Certbot](https://certbot.eff.org/lets-encrypt/) SSL Renewal

A set of shell scripts to automate Let's Encrypt SSL certificate renew and GitLab pages certificate updates.

See [Let's Encrypt GitLab Again...](https://autonomic.guru/using-letsencrypt-on-gitlabs/) 

### Setup

**Requirements**

  * [jq v1.5](https://stedolan.github.io/jq/) – a lightweight and flexible command-line JSON processor.
  * [CertBot v0.25.1](https://certbot.eff.org/) – Automatically enable HTTPS on your website with EFF's Certbot, deploying [Let's Encrypt](https://letsencrypt.org/) certificates.
  * [Curl v7.54](https://curl.haxx.se/) – command line tool and library for transferring data with URLs

**Configuration**

Before running, you'll need to modify the following 3 files:

  * `certbot-example.ini` – Certbot configuration.  Update domain(s) and email address.
  * `gitlab-auth-hook.sh` – Authorization and validation hook.  Update git repository and Let's Encrypt validation path settings.
  *  `gitlab-put-certs.sh` – Certificate publishing hook.  Update with CertBot live certificate path, your GitLab pages project id, and GitLab API token.

----

### Usage

```
certbot certonly --manual --preferred-challenges=http -n --manual-public-ip-logging-ok --config-dir ./ --work-dir ./ --manual-auth-hook ./gitlab-auth-hook.sh --manual-cleanup-hook ./gitlab-put-certs.sh --config [CERTBOT.ini]
```

Replace `[CERTBOT.ini]` with the CertBot configuration file for your domain(s) hosted on GitLab pages.  Above and the example below assume that you are running certbot from the directory where the hooks are stored.  When run a several directories will be created and populated by certbot.  

**Example Run** 

```bash
$ certbot certonly --manual --preferred-challenges=http -n --manual-public-ip-logging-ok --config-dir ./ --work-dir ./ --manual-auth-hook ./gitlab-auth-hook.sh --manual-cleanup-hook ./gitlab-put-certs.sh --config example-com.ini

Saving debug log to /tmp/certbot/logs/letsencrypt.log
Plugins selected: Authenticator manual, Installer None
Renewing an existing certificate
Performing the following challenges:
http-01 challenge for example.com
http-01 challenge for www.example.com
Output from gitlab-auth-hook.sh:
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean

Error output from gitlab-auth-hook.sh:
Everything up-to-date

Output from gitlab-auth-hook.sh:
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean

Error output from gitlab-auth-hook.sh:
Everything up-to-date

Waiting for verification...
Cleaning up challenges

Output from gitlab-put-certs.sh:
Updating certificate for: example.com
{
  "domain": "example.com",
  "url": "https://example.com",
  "verified": true,
  "verification_code": "47969277390e60fddc22a4e2efd46f83",
  "enabled_until": "2018-06-29T03:45:36.047Z",
  "certificate": {
[OUTPUT TRUNCATED]
  }
}
Updating certificate for: www.example.com
{
  "domain": "www.example.com",
  "url": "https://www.example.com",
  "verified": true,
  "verification_code": "bae004a790b116f18f54fc25dab9a983",
  "enabled_until": "2018-06-29T23:00:31.921Z",
  "certificate": {
[OUTPUT TRUNCATED]
  }
}


IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /home/user/gitlab-ssl-renew/live/example.com/fullchain.pem
   Your key file has been saved at:
   /home/user/gitlab-ssl-renew/live/example.com/privkey.pem
   Your cert will expire on 2018-09-22. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le   
```

